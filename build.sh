#!/bin/sh

# Create ConfigMap with prometheus rules for alerting
kubectl --namespace monitoring create configmap --dry-run prometheus-rules \
  --from-file=configs/prometheus/rules \
  --output yaml \
    > ./manifests/prometheus/config-prometheus-rules.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap for an external url
kubectl --namespace monitoring create configmap --dry-run alertmanager-templates \
  --from-file=configs/alertmanager-templates \
  --output yaml \
    > ./manifests/alertmanager/alertmanager-templates.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Grafana dashboards and datasources
kubectl --namespace monitoring create configmap --dry-run grafana-import-dashboards \
  --from-file=configs/grafana \
  --output yaml \
    > ./manifests/grafana/import-dashboards/configmap.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Prometheus config
kubectl --namespace monitoring create configmap --dry-run prometheus-hass \
  --from-file=configs/prometheus/prometheus-hass.yaml \
  --output yaml \
    > ./manifests/prometheus/configmap-hass.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Prometheus config
kubectl --namespace monitoring create configmap --dry-run prometheus-view \
  --from-file=configs/prometheus/prometheus-view.yaml \
  --output yaml \
    > ./manifests/prometheus/configmap-view.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Prometheus config
kubectl --namespace monitoring create configmap --dry-run prometheus-system \
  --from-file=configs/prometheus/prometheus-system.yaml \
  --output yaml \
    > ./manifests/prometheus/configmap-system.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Prometheus config
kubectl --namespace monitoring create configmap --dry-run prometheus-iot \
  --from-file=configs/prometheus/prometheus-iot.yaml \
  --output yaml \
    > ./manifests/prometheus/configmap-iot.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Prometheus config
kubectl --namespace monitoring create configmap --dry-run prometheus-longlive \
  --from-file=configs/prometheus/prometheus-longlive.yaml \
  --output yaml \
    > ./manifests/prometheus/configmap-longlive.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create ConfigMap with Prometheus config
kubectl --namespace monitoring create configmap --dry-run prometheus-medlive \
  --from-file=configs/prometheus/prometheus-medlive.yaml \
  --output yaml \
    > ./manifests/prometheus/configmap-medlive.yaml
# Workaround since `--namespace monitoring` from above is not preserved

# Create one single manifest file
target="./manifests-all.yaml"
rm "$target"
echo "# Derived from ./manifests" >> "$target"
for file in $(find ./manifests -type f -name "*.yaml" | sort -V) ; do
  echo "---" >> "$target"
  cat "$file" >> "$target"
done
